import logging
import json
import yaml
from tableschematransformer.core import TableSchemaTransformer
from tableschema import validate, exceptions
from tableschema import Schema



def test_conn():
    c = TableSchemaTransformer('tests/ts-example1.yaml')
    logging.info(c.generate_spark_source('the.package'))

def test_json():
    with open('tests/ts-example1.json') as f:
        descriptor = json.load(f)
    for r in descriptor["resources"]:
        schema = Schema(r['schema'])
        try:
            valid = validate(schema.descriptor)
        except exceptions.ValidationError as exception:
            logging.error(exception.errors)

def test_yaml():
    with open('tests/ts-example1.yaml') as f:
        descriptor = yaml.load(f, Loader=yaml.FullLoader)
    for r in descriptor["resources"]:
        schema = Schema(r['schema'])
        try:
            valid = validate(schema.descriptor)
        except exceptions.ValidationError as exception:
            logging.error(exception.errors)

