from tableschematransformer.core import TableSchemaTransformer

import argparse

parser = argparse.ArgumentParser(prog='PROG')
parser.add_argument("--package", required=True)
parser.add_argument("--file", required=True)
ns = parser.parse_args()

package = ns.package
filePath = ns.file

tst = TableSchemaTransformer(filePath)
print(tst.generate_spark_source(package))

